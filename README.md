**Accounts Setup**

*Discord*
Head to http://www.discord.com and download the app, register an account and click this <invite link> to join the Interface Agency server

*Bitbucket*
Head to http://www.bitbucket.org and register an account with your Interface Agency email.

*Interface Agency Email*
Head to http://mail.hostedmail.com/ and log in with your email and password

**Installing Programs**

*Install Firefox*
This can be downloaded from http://www.mozilla.org

*Install GIT*
Download this here: https://github.com/git-for-windows/git/releases/download/v2.27.0.windows.1/Git-2.27.0-64-bit.exe
and install, following the instructions on screen

*Install repositories - terminal alias, atom-shared settings*
Open Git Bash from the start menu (Search Git Bash)
Once open type
```bash
cd ~
```
followed by
```bash
mkdir Development
```
Next, type
```bash
git clone git@bitbucket.org:interface-agency/terminal-alias.git
```
and
```bash
git clone git@bitbucket.org:interface-agency/atom-settings.git
```

*Install NPM*
Head to https://www.npmjs.com/get-npm and follow the instructions to get Node and NPM installed

*Install Atom*
Head to https://atom.io and download, and then install Atom.

*Add SSH key to Bitbucket*
Open Windows Powershell (Search for this in the start menu)
Type
```bash
ssh-keygen
```
And press **Enter** *3 times* to complete the generation process

Next, type 
```bash
eval $(ssh-agent)
```
Followed by

```bash
ssh-add ~/.ssh/id_rsa
```

Then type 
```bash
cat ~/.ssh/id_rsa.pub | pbcopy .
```
Visit https://bitbucket.org/account/settings/ssh-keys/ and paste it into the KEY field, then click **Add Key**

*Add shared settings to Atom*

__**Git Global Settings**__
Add the following to your git global config by running these commands:
```bash
git config --global submodule.recurse true
git config --global user.name "Name and Surname"
git config --global user.email name.surname@email.com
```
